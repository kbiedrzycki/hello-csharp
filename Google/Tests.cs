using System;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

using OpenQA.Selenium;

namespace example_client
{
    namespace Google
    {
        [TestFixture]
        public class Tests
        {
            readonly Browser browser = new Browser();
            private IWebDriver driver;

            [SetUp]
            public void SetUp()
            {
                browser.Initialize();
            }

            [TearDown]
            public void TearDown()
            {
                browser.Close();
            }

            [Test]
            public void GoogleTest()
            {
                browser.Goto("https://google.com");

                driver = browser.Driver;

                string title = driver.Title;

                Assert.AreEqual(title, "Google");
            }

            [Test]
            public void AnyPageTest()
            {
                string inputParameters = Environment.GetEnvironmentVariable("inputParameters");

                Console.WriteLine(inputParameters);

                dynamic parsedInputParameters = JObject.Parse(inputParameters);

                Console.WriteLine(parsedInputParameters);
                Console.WriteLine(parsedInputParameters["testTargetURL"]);
                Console.WriteLine(parsedInputParameters["expectedTitle"]);

                browser.Goto(Convert.ToString(parsedInputParameters["testTargetURL"]));

                driver = browser.Driver;

                string title = driver.Title;

                Assert.AreEqual(title, Convert.ToString(parsedInputParameters["expectedTitle"]));
            }
        }
    }
}
